# Boilerplate ReactJS Lean Tech

This repository describe all the work plan, code implemented and documentation about the conventional stack to work with the ReactJS inside the company.

## Why

The create-react-app-leantech o ReactJS Boilerplate, is a Stack of the work with REACT, is a standard of code and contain architectural instructions, agile tools prepared and a core dedicated a environment of production, following and management of results used in the web development.

### Libreries and Technologies

| Name | Priority | Implemented | Resource |
| ---------- | ---------- | ---------- | ---------- |
| Framework CSS | Hight | 100% | https://evergreen.segment.com/ |
| ErrorBoundary | Hight | 100% | - - |
| Redux | Hight | 100% | - - |
| Redux Persistor | Hight | 100% | - - |
| Redux Saga | Hight | 100% | - - |
| Service REST | Hight | 100% | - - |
| Axios | Hight | 100% | - - |
| Theme application | Medium | 100% | - - |
| Component generic text | Low | 100% | - - |
| Switch component | Medium | 100% | - - |
| Error logs | Medium | 70% | - - |
| Support Tickets | Medium | 70% | - - |
| Firebase | Low | 50% | - - |
| Validatator file | Low | 0% | - - |
| Lang file | Low | 0% | - - |
| Moment | Low | 0% | - - |
| Opcional TypeScript | Low | 0% | - - |
| Formik | Low | 0% | - - |
| Yup | Low | 0% | - - |
| Satisfaction Dialog | Low | 0% | - - |
| Analytics | Low | 0% | - - |
| Push Notification | Medium | 0% | - - |
| Graphql | Low | 0% | - - |
| React-cookies | Medium | 0% | - - |
| Despliegue Continuo | Medium | 0% | - - |
| Screens Errors | Medium | 0% | - - |
| Auth0 | Medium | 0% | - - |
| Unit Test | Hight | 0% | - - |
| Pipelines | Higth | 0% | - - |
| Documentation | Hight | 40% | - - |

### Team

| Name | Days | Hours | Total Hours X Week |
| ---------- | ---------- | ---------- | ---------- |
| Diego Contreras | M T W T | 1.5 hours | 6 hours |
| Patricia Montoya | M T W T | 1 hours | 4 hours |
| Brian Bernal | M T W T | 1 hours | 4 hours |
| Edwin Anaya | M T W T | 2 hours | 8 hours |